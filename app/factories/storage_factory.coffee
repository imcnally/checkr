set = (key, value) ->
  localStorage.setItem key, if value then JSON.stringify(value) else value

get = (key, value) ->
  JSON.parse localStorage.getItem key

bind = (scope, variableNameAsString) ->
  scope.$watch variableNameAsString, (value) ->
    set variableNameAsString, value

app.factory "Storage", ->

  {set, get, bind}
