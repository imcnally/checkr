app.controller "AppController", ($scope, Payer, constants) ->

  # to-do: only assignment is trigger event in Storage
  # not element in array changes, or pushing to an array
  # so, fix that and have persistance
  $scope.payers or= []

  $scope.tax or= constants.DEFAULT_TAX

  $scope.payersDues or= []

  $scope.addPayer = ->
    $scope.payers = $scope.payers.concat [new Payer]

  $scope.clearPayers = ->
    $scope.payers = []

  $scope.calculate = ->
    $scope.payersDues = []

    tax = parseFloat $scope.tax

    duePlusTax = (due) ->
      due = parseFloat due
      parseFloat (due + (due * tax / 100)).toFixed(2)

    composeTotal = (memo, payer) -> parseFloat memo + duePlusTax(payer.pretaxDue)

    grandTotal = parseFloat _.reduce($scope.payers, composeTotal, 0).toFixed(2)
    remainingTotal = grandTotal

    for payer, idx in $scope.payers
      name = payer.name
      amount =
        if idx isnt $scope.payers.length - 1
          due = duePlusTax(payer.pretaxDue)
          remainingTotal -= due
          due
        else
          remainingTotal.toFixed 2
      $scope.payersDues.push {name, amount}
