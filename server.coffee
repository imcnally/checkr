express = require "express"
fs = require "fs"
app = express()
port = process.env.PORT or 5000
{build} = require "./helpers"
$ = require "jquery"

PUBLIC_DIR = __dirname + "/public"
APP_DIR = __dirname + "/app"
APP_3P_DIR = APP_DIR + "/3p"

# listen on port 5000
app.listen port, ->
  console.log "Listening on #{port}\n"

# set app configuration
app.configure ->
  # compile stylus to css on the fly
  app.use require("stylus").middleware
    # app/stylesheets
    src : APP_DIR
    # public/stylesheets
    dest : PUBLIC_DIR

  # serve public dir
  app.use express.static PUBLIC_DIR

# Build run-time assets
build()

# landing page
app.get "/", (req, res) =>
  res.send fs.readFileSync("app/views/index.html").toString()
