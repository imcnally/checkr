## Checkr

Checkr is for when the check comes after dinner with a group of friends. Not just any friends, efficient friends who like to calculate who much everyone owes, tax included, and down to the penny.

Enter your friends' names, and their pre-tax total, and Checkr will calculate how much the server should put on everyone's cards. You've got better things to do than scribble on a receipt and tally it up on your phone.
